
package controller;

import catalog.entity.CatalogItem;
import catalog.entity.EntityFactoryLocal;
import catalog.service.CatalogServiceLocal;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;


@Named
@RequestScoped
public class CatalogController {

    @Inject
    EntityFactoryLocal entityFactoryLocal;

    @Inject
    CatalogServiceLocal catalogServiceLocal;

    
    CatalogItem catalogItem;

    public void initData() {
        catalogServiceLocal.addItem(entityFactoryLocal.createCatalogItem("AZ749", "Tomate", "Auchan"));
        catalogServiceLocal.addItem(entityFactoryLocal.createCatalogItem("RT475", "Banane", "Carrefour"));
    }
    public void initCatalogItem() {
        catalogItem = entityFactoryLocal.createCatalogItem();
    }

    public List<CatalogItem> getItems() {
        return catalogServiceLocal.getItems();
    }

    public String addItem() {
        catalogServiceLocal.addItem(catalogItem);
        return "product_list?faces-redirect=true";
    }

    public CatalogItem getCatalogItem() {
        return catalogItem;
    }

    public void setCatalogItem(CatalogItem catalogItem) {
        this.catalogItem = catalogItem;
    }
}
