package catalog.service;

import java.util.List;

import javax.ejb.Local;

import catalog.entity.CatalogItem;

@Local
public interface CatalogServiceLocal {
	 public List<CatalogItem> getItems();
	 public void addItem(CatalogItem item);

}
