package catalog.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import catalog.entity.CatalogItem;

/**
 * Session Bean implementation class CatalogService
 */
@Stateless
public class CatalogService implements CatalogServiceLocal {
    @PersistenceContext
    private EntityManager em;
  

    public List<CatalogItem> getItems() {
        TypedQuery<CatalogItem> query = em.createQuery("select c from CatalogItem c", CatalogItem.class);
        return query.getResultList();
    }

    public void addItem(CatalogItem item) {
        em.persist(item);
    }

}
