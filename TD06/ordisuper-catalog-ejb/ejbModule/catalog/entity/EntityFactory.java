package catalog.entity;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * Session Bean implementation class EntityFactory
 */
@Singleton
@Startup
@Lock(LockType.READ)

public class EntityFactory implements EntityFactoryLocal {

    public CatalogItem createCatalogItem() {
        return new CatalogItem();
    }

    public CatalogItem createCatalogItem(String sku, String name, String manufacturer) {
        return new CatalogItem(sku, name, manufacturer);
    }
}
