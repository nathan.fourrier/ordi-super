package catalog.entity;

import javax.ejb.Local;

@Local
public interface EntityFactoryLocal {
	public CatalogItem createCatalogItem();
    public CatalogItem createCatalogItem(String sku, String name, String manufacturer);

}
