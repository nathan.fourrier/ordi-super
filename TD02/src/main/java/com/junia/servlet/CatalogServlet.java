package com.junia.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/CatalogServlet")
public class CatalogServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CatalogServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String name = request.getParameter("name");
		response.getWriter().append(name);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String sku = request.getParameter("sku");
		String name = request.getParameter("name");
		String manufacturer = request.getParameter("manufacturer");

		Catalog.addItem(new CatalogItem(sku, name, manufacturer));

		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<meta charset=\"UTF-8\">");
		out.println("<title>CatalogServlet</title>");
		out.println("</head>");
		out.println("<body>");

		out.println("<table>");
		for (CatalogItem item : Catalog.getItems()) {
			out.println("<tr><td>" + item.getName() + " / " + item.getManufacturer() + " / " + item.getSku()
					+ "</td></tr>");
		}
		out.println("</table>");
		out.println("<br /><a href=\"form.html\">Saisir un nouveau produit</a>");
		out.println("</body>");
		out.println("</html>");
	}

}
