<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Liste des produits</title>
<link rel="stylesheet" href="./style.css">
</head>
<body>

	<div class="wrapper">

		<div class="menu">
			<div class="libelle">
				<a href="<c:url value="CatalogServlet" />">Nouveau</a>
			</div>
			<div class="sep">|</div>
			<div class="libelle">
				<a href="#">Liste</a>
			</div>
			<div class="sep">|</div>
			<div class="libelle">
				<a href="#">Recherche</a>
			</div>
			<div class="sep">|</div>
			<div class="libelle">
				<a href="#">Chat</a>
			</div>
		</div>

		<div class="image"></div>

		<table>
			<thead>
				<tr>
					<td>SKU</td>
					<td>NAME</td>
					<td>MANUFACTURER</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${items}">
					<tr>
						<td>${item.sku}</td>
						<td>${item.name}</td>
						<td>${item.manufacturer}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>

</body>
</html>