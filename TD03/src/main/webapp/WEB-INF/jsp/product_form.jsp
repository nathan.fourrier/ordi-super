<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>OrdiSuper - Admin</title>
    <link rel="stylesheet" href="./style.css">
  </head>
  <body>
  
	<div class="wrapper">

		<div class="menu">
			<div class="libelle"><a href="#">Nouveau</a></div>
			<div class="sep">|</div>
			<div class="libelle"><a href="#">Liste</a></div>
			<div class="sep">|</div>
			<div class="libelle"><a href="#">Recherche</a></div>
			<div class="sep">|</div>
			<div class="libelle"><a href="#">Chat</a></div>
		</div>
		
		<div class="image"></div>
		
		<form action="<c:url value="CatalogServlet" />" method="POST">
			<div class="input">
				<div class="libelle">Libell� produit :</div>
				<div class="composant"><input type="text" name="name" /></div>
			</div>
			<div class="input">
				<div class="libelle">Fabricant :</div>
				<div class="composant"><input type="text" name="manufacturer" /></div>
			</div>
			<div class="input">
				<div class="libelle">SKU :</div>
				<div class="composant"><input type="text" name="sku" /></div>
			</div>
			<div class="envoyer"><input type="submit" value="Enregistrer" /></div>
		</form>
	
	</div>

  </body>
</html>