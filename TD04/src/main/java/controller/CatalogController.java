
package controller;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import helper.Catalog;
import model.CatalogItem;

@Named
@RequestScoped
public class CatalogController {

	private CatalogItem catalogItem = new CatalogItem();

	public String addNewItem() {

		// ajout d'un nouveau produit au catalogue
		Catalog.addItem(catalogItem);

		// et redirige vers la liste des produits
		return "product_list";

	}

	public CatalogItem getCatalogItem() {
		return catalogItem;
	}

	public void setCatalogItem(CatalogItem catalogItem) {
		this.catalogItem = catalogItem;
	}

	public List<CatalogItem> getCatalogItems() {
		return Catalog.getItems();
	}

}
