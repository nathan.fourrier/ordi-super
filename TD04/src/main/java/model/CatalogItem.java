package model;

import javax.validation.constraints.Size;

/**
 * Produit du catalogue.
 * 
 *
 */
public class CatalogItem {

	/**
	 * Identifiant SKU.
	 */
	@Size(min = 1, max = 5)
	private String sku;

	/**
	 * Libellé du produit.
	 */
	@Size(min = 1, max = 64)
	private String name;

	/**
	 * Nom du fournisseur.
	 */
	@Size(min = 1, max = 64)
	private String manufacturer;

	/**
	 * Constructeur par défaut.
	 */
	public CatalogItem() {

	}

	/**
	 * Constructeur
	 * 
	 * @param sku          Identifiant SKU du produit.
	 * @param name         Libellé du produit.
	 * @param manufacturer Nom du fournisseur.
	 */
	public CatalogItem(String sku, String name, String manufacturer) {
		super();
		this.sku = sku;
		this.name = name;
		this.manufacturer = manufacturer;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

}
