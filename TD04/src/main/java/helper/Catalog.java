package helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.CatalogItem;

/**
 * Classe utilitaire qui gère le catalogue de produits.
 *
 */
public class Catalog {

	private static Map<String, CatalogItem> catalogItems = new HashMap<>();

	/**
	 * Ajoute un produit au catalogue.
	 * 
	 * @param catalogItem Produit � ajouter.
	 */
	public static void addItem(CatalogItem catalogItem) {
		catalogItems.put(catalogItem.getSku(), catalogItem);
	}

	/**
	 * Retourne un produit selon le SKU sp�cifi�.
	 * 
	 * @param sku SKU du produit � rechercher.
	 * @return Le produit recherch�.
	 */
	public static CatalogItem getItem(String sku) {
		return catalogItems.get(sku);
	}

	/**
	 * Retourne la liste de tous les produits du catalogue.
	 * 
	 * @return La liste des produits.
	 */
	public static List<CatalogItem> getItems() {
		return new ArrayList<CatalogItem>(catalogItems.values());
	}

}
