
package controller;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.DataService;
import model.entity.CatalogItem;

@Named
@RequestScoped
public class CatalogController {

	@Inject
	private DataService dataService;

	private CatalogItem item = new CatalogItem();

	private List<CatalogItem> items = new ArrayList<>();

	public String addNewItem() {

		// ajout d'un nouveau produit au catalogue
		dataService.addItem(item);

		// et redirige vers la liste des produits
		return "product_list?faces-redirect=true";

	}

	public void init() {
		items = dataService.getItems();
	}

	public CatalogItem getItem() {
		return item;
	}

	public void setCatalogItem(CatalogItem catalogItem) {
		this.item = catalogItem;
	}

	public List<CatalogItem> getCatalogItems() {
		return items;
	}

}
