package model;

import java.util.List;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;

import model.entity.CatalogItem;

@Named
@ApplicationScoped
@Transactional
public class DataService {

	@PersistenceContext
	private EntityManager em;

	@Resource
	private UserTransaction userTransaction;

	public List<CatalogItem> getItems() {
		return em.createQuery("select i from CatalogItem i", CatalogItem.class).getResultList();
	}

	public void addItem(CatalogItem item) {
		em.persist(item);
	}

}
