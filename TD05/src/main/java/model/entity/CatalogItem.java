package model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * Produit du catalogue.
 *
 */
@Entity
@Table(name = "catalog_item")
public class CatalogItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * Identifiant SKU.
	 */
	@Size(min = 1, max = 5)
	private String sku;

	/**
	 * Libell� du produit.
	 */
	@Size(min = 1, max = 64)
	private String name;

	/**
	 * Nom du fournisseur.
	 */
	@Size(min = 1, max = 64)
	private String manufacturer;

	/**
	 * Constructeur par d�faut.
	 */
	public CatalogItem() {

	}

	/**
	 * Constructeur
	 * 
	 * @param sku          Identifiant SKU du produit.
	 * @param name         Libell� du produit.
	 * @param manufacturer Nom du fournisseur.
	 */
	public CatalogItem(String sku, String name, String manufacturer) {
		super();
		this.sku = sku;
		this.name = name;
		this.manufacturer = manufacturer;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

}
